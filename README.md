# Toko gerobak mebel
Toko gerobak mebel merupakan aplikasi web toko online sederhana dengan demo produk penjualan gerobak yang berkualitas bagus 


##Fitur
- Login
- Register
- Keranjang belanja
- Checkout
- Sistem kupon Belanja
- Pembayaran melalui transfer bank
- Konfirmasi pembayaran
- Formulir kontak
- Testimoni

##Hak akses
- Admin
- Customer

##User demo:
Untuk mencoba menggunakan toko ini, gunakan dua browser yang berbeda untuk memudahkan (Chrome atau Firefox) dan login dengan akun berikut:

**Admin:**
- Username: admin
- Password: 12345

**Customer:**
- Username: customer
- Password: 12345


